function lab1()
    clc;
    C_0 = [1 1 1 1 1; 
           1 10 9 8 7; 
           1 8 10 4 3; 
           1 9 11 3 6; 
           1 11 3 6 9];
    
    isMaxTask = 0; % 1 - максимизация, 0 - минимизация

    if isMaxTask == 1
        C = maximizationPreprocessing(C_0);
    else
        C = C_0;
    end   
       
    fprintf('Начальная матрица стоимостей: \n');
    disp(C);
       
    C = columnReduction(C);
    fprintf('Редукция столбцов: \n');
    disp(C);
    
    C = rowReduction(C);
    fprintf('Редукция строк: \n');
    disp(C);
    
    size = length(C);
    elementsWithStar = initialize(C);
    elementsWithQuotes = zeros(size, size);
    
    iteration = 1;
    while nnz(elementsWithStar) ~= length(C)
        fprintf('Итерация %d\n', iteration);
        selectedRows = [];
        selectedColumns = getInitialSelectedColumns(elementsWithStar);
        fprintf('Начальная матрица СНН: \n');
        printMatrix(C, elementsWithStar, elementsWithQuotes, selectedRows, selectedColumns);
        while 1
            [i, j] = findUnselectedZero(C, selectedRows, selectedColumns);
            if isequal([i, j], [-1, -1])
                C = getNewZeroElements(C, selectedRows, selectedColumns);
            else
                elementsWithQuotes(i, j) = 1;
                if isRowBusyWithStar(i, elementsWithStar)
                    selectedColumns = selectedColumns(selectedColumns ~= getColumnWithStar(i, elementsWithStar));
                    selectedRows = [selectedRows, i];
                    printMatrix(C, elementsWithStar, elementsWithQuotes, selectedRows, selectedColumns);
                else
                    printMatrix(C, elementsWithStar, elementsWithQuotes, selectedRows, selectedColumns);
                    elementsWithStar = LChain(elementsWithStar, elementsWithQuotes, i, j);
                    elementsWithQuotes = zeros(size, size);
                    break;
                end
            end
        end
        iteration = iteration + 1;
    end
    selectedColumns = getInitialSelectedColumns(elementsWithStar);
    fprintf('Финальная матрица СНН: \n');
    printMatrix(C, elementsWithStar, elementsWithQuotes, selectedRows, selectedColumns);
    getSolutionAnswer(elementsWithStar, C_0);
end

% редукция столбцов
function C_new = columnReduction(C)
    minColumn = min(C);
    C_new = C - minColumn;
end

% редукция строк
function C_new = rowReduction(C)
    minRow = min(C');
    C_new = (C' - minRow)';
end

% предварительная обработка для задачи максимизации 
function C_new = maximizationPreprocessing(C)
    maxColumn = max(C);
    C_new = maxColumn - C;
end

% инициализация 
function elementsWithStar = initialize(C)
    size = length(C);
    elementsWithStar = zeros(size, size); 
    for j = 1 : size
        for i = 1 : size
            if C(i, j) == 0 && ~isRowBusyWithStar(i, elementsWithStar)
                elementsWithStar(i, j) = 1;
                break;
            end
        end
    end
end

% проверка есть ли в строке 0*
function result = isRowBusyWithStar(i, elementsWithStar)
    result = false;
    size = length(elementsWithStar);
    for j = 1 : size
        if elementsWithStar(i, j) == 1
            result = true;
            break;
        end
    end
end

% получить индекс столбца с элементом 0' в заданной строке
function result = getColumnWithQuote(i, elementsWithQuotes)
    size = length(elementsWithQuotes);
    result = -1;
    for j = 1 : size
        if elementsWithQuotes(i, j) == 1
            result = j;
        end
    end
end

% получить индекс столбца с элементом 0* в заданной строке
function result = getColumnWithStar(i, elementsWithStar)
    size = length(elementsWithStar);
    result = -1;
    for j = 1 : size
        if elementsWithStar(i, j) == 1
            result = j;
        end
    end
end

% получить индекс строки с элементом 0* в заданном столбце
function result = getRowWithStar(j, elementsWithStar)
    size = length(elementsWithStar);
    result = -1;
    for i = 1 : size
        if elementsWithStar(i, j) == 1
            result = i;
        end
    end
end

% проверка есть ли в столбце 0*
function result = isColumnBusyWithStar(j, elementsWithStar)
    result = false;
    size = length(elementsWithStar);
    for i = 1 : size
        if elementsWithStar(i, j) == 1
            result = true;
            break;
        end
    end
end

% инициализация списка выделенных элементов
function selectedColumns = getInitialSelectedColumns(elementsWithStar)
    selectedColumns = [];
    size = length(elementsWithStar);
    for j = 1 : size
        if isColumnBusyWithStar(j, elementsWithStar)
            selectedColumns = [selectedColumns, j];
        end
    end
end

% нахождение нулей среди невыделенных элементов
function [unselectedZero_x, unselectedZero_y] = findUnselectedZero(C, selectedRows, selectedColumns)
    size = length(C);
    unselectedZero_x = -1;
    unselectedZero_y = -1;
    for i = 1 : size
        if ~ismember(i, selectedRows)
            for j = 1 : size
                if ~ismember(j, selectedColumns) && C(i, j) == 0
                    unselectedZero_x = i;
                    unselectedZero_y = j;
                    return;
                end     
            end
        end
    end
end

% построение L - цепочки
function elementsWithStar_new = LChain(elementsWithStar, elementsWithQuotes, i, j)
    elementsWithStar_new = elementsWithStar;
    fst = 0;
    while 1
        i_new = getRowWithStar(j, elementsWithStar);
        elementsWithStar_new(i, j) = 1;
        if fst == 0
            fprintf('L-цепочка: (%d, %d) ', i, j);
            fst = 1;
        else
            fprintf('-> (%d, %d) ', i, j);
        end
        if i_new == -1
            break;
        end
        i = i_new;
        j_new = getColumnWithQuote(i, elementsWithQuotes);
        fprintf('-> (%d, %d) ', i, j);
        elementsWithStar_new(i, j) = 0;
        if j_new == -1
            break;
        end
        j = j_new;
    end
    fprintf('\n');
end

% нахождение минимального не нулевого элемента среди невыделенных элементов
function minElement = findMinInSelectedSection(C, selectedRows, selectedColumns)
    size = length(C);
    minElement = max(max(C));
    for i = 1 : size
        if ~ismember(i, selectedRows)
            for j = 1 : size
                if ~ismember(j, selectedColumns) && C(i, j) ~= 0 && C(i, j) < minElement
                    minElement = C(i, j);
                end
            end
        end
    end
end

% Получение новых нулевых элементов среди невыделенных жлементов
function C = getNewZeroElements(C, selectedRows, selectedColumns)
    size = length(C);
    minElement = findMinInSelectedSection(C, selectedRows, selectedColumns);

    for j = 1 : size
        if ~ismember(j, selectedColumns)
            C(:, j) = C(:, j) - minElement;
        end
    end
    
    for i = 1 : size
        if ismember(i, selectedRows)
            C(i, :) = C(i, :) + minElement;
        end
    end
end

% вывод элемента матрицы
function printMatrixElement(C, i, j, string, selectedRows, selectedColumns)
    if ismember(i, selectedRows) || ismember(j, selectedColumns)
        fprintf(2, string, C(i, j));
    else
        fprintf(string, C(i, j));
    end
end

% вывод матрицы
function printMatrix(C, elementsWithStar, elementsWithQuotes, selectedRows, selectedColumns)
    size = length(C);
    for i = 1 : size
        for j = 1 : size
            if elementsWithStar(i,j) ~= 0
                printMatrixElement(C, i, j, '%d* ', selectedRows, selectedColumns)
            elseif elementsWithQuotes(i,j) ~= 0
                printMatrixElement(C, i, j, "%d' ", selectedRows, selectedColumns)
            else
                printMatrixElement(C, i, j, '%d  ', selectedRows, selectedColumns)
            end
        end
        fprintf('\n');
    end
    fprintf('\n');
end

% вывести результат
function getSolutionAnswer(elementsWithStar, C)
    x_opt = elementsWithStar;
    disp('x_opt = ')
    disp(x_opt);
    y_opt = 0;
    size = length(C);
    for i = 1 : size
        for j = 1 : size
            y_opt = y_opt + x_opt(i,j) * C(i,j);
        end
    end
    fprintf('y_opt = %d\n\n', y_opt);    
end
